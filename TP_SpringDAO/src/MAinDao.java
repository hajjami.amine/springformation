import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

import com.formation.modele.Adherent;
import com.formation.service.IAdherentService;


public class MAinDao {
	public static void main(String args[]){
		ClassPathResource cpr=new ClassPathResource("applicationContext.xml"); 
		 
		// Initialisation de la fabrique SPRING, les singletons sont charges 
		ListableBeanFactory bf = new XmlBeanFactory(cpr); 
		 
		// Initialisation de la methode getBean en parametrant le nom du bean 
		IAdherentService sa = (IAdherentService) bf.getBean("gestionAdherent");
		System.out.println(sa.getAllAdherent());
		

        Adherent a2=new Adherent("C200","Feca","Sasuri ","Dofus"); 
        Adherent a3=new Adherent("C300","Master","Yi","League"); 
        sa.addAdherent(a2);
        sa.addAdherent(a3);
        System.out.println(sa.getAdherentByCode("C300"));
        //sa.removeAdherent("C300");
	}
}
