package cat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan(basePackages = {"cat.entities"})
@ComponentScan
public class CatsApplication {
	public static void main(String[] args){
		SpringApplication.run(CatsApplication.class, args);
	}
}
