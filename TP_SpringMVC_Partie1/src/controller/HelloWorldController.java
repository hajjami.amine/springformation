package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HelloWorldController {
	@RequestMapping("/index")
    public ModelAndView hello(){
    	String msg = "Ma premiere Page Spring MVC";
    	return new ModelAndView("hello","message",msg);
    }

}
