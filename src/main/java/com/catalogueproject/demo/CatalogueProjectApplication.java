package com.catalogueproject.demo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = {"entity"})  // scan JPA entities
public class CatalogueProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(CatalogueProjectApplication.class, args);
    }

}

